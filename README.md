# tvos-gc-issue

This project replicates an issue I've been having with the `GameController` Framework on tvOS.

You're supposed to register for `GCControllerDidConnect` notifications before checking the `GCController.controllers()` Array.

However this doesn't seem to work reliably and only works right after the application launches for me for some reason.

I've tested this on two different Apple TV 4Ks.

# how to replicate

All you need to change is [this](https://gitlab.com/ckaik/tvos-gc-issue/-/blob/main/GCIssue/ViewController.swift#L10) line.

Remove the `+ 1` delay and it should work.

Leave it and you don't receive the notification.
