import UIKit
import GameController

class ViewController: UIViewController {
	override func viewDidLoad() {
		super.viewDidLoad()

		// It doesn't work with that 1 sec delay
		// If you remove that, the notification is received
		DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
			self.registerForControllerNotifications()
		}
	}

	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)

		NotificationCenter.default.removeObserver(self, name: .GCControllerDidConnect, object: nil)
	}

	func registerForControllerNotifications() {
		print("Register for Notifications")

		NotificationCenter.default.addObserver(self, selector: #selector(controllerConnected(_:)), name: .GCControllerDidConnect, object: nil)
	}

	@objc func controllerConnected(_ note: Notification) {
		print("Controller Connected")

		GCController.controllers().forEach { print("Controller: \($0)") }
	}
}

